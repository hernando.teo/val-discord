#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'

abort "#{__FILE__} username file" if ARGV.size < 2

username = ARGV.first
file = ARGV[1]

yaml = YAML.load_stream(open(file))

formatted =
    yaml.map do |x|
        info = x['message'].split(/ @ | # | wrote:| edited:/)
        {
            timestamp: x['timestamp'],
            username:  info.first,
            server:    info[1],
            channel:   info[2],
            message:   info.last
        }
    end

filtered = formatted.select { |x| x[:username] == username }

filtered.each do |x|
    puts "Timestamp: #{x[:timestamp]}"
    puts "Username:  #{x[:username]}"
    puts "Server:    #{x[:server]}"
    puts "Channel:   #{x[:channel]}"
    puts "Message:   #{x[:message]}"
    puts '---'
end
