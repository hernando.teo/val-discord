#!/usr/bin/env bash

unset CDPATH

cd "$(dirname $0)"

export PATH="$HOME/.rbenv/bin:$PATH"

if type -P rbenv > /dev/null; then
   eval "$(rbenv init -)"
fi

exec bin/rake run
