FROM ruby:2.3.3

ENV USER appuser
ENV UID 1000
ENV APP_ROOT /app

# dependencies and user setup
RUN echo "deb http://ftp.uk.debian.org/debian jessie-backports main" >> /etc/apt/sources.list \
 && apt-get update -qq \
 && apt-get install -y --no-install-recommends \
    build-essential \
    libpq-dev \
    nodejs \
	nodejs-legacy \
    nacl-tools \
    opus-tools \
    vorbis-tools \
    ffmpeg \
 && useradd -m -s /bin/bash -u ${UID} ${USER}

# app setup
RUN mkdir ${APP_ROOT}
WORKDIR ${APP_ROOT}

ADD Gemfile Gemfile
ADD Gemfile.lock Gemfile.lock
RUN bundle install
ADD . .
RUN chown -R ${USER}:${USER} ${APP_ROOT}

USER ${USER}
