# frozen_string_literal: true

require 'active_record_migrations'

dbconf =
    if File.exist?('config/database.yml')
        'config/database.yml'
    else
        'config/default/database.yml'
    end

ActiveRecordMigrations.configure do |conf|
    conf.yaml_config = dbconf
    conf.environment = ENV['KYOUKO_ENV'] || 'development'
end

ActiveRecordMigrations.load_tasks

ENV['LD_LIBRARY_PATH'] = File.join(__dir__, 'vendor/lib')
ENV['PATH'] = "#{__dir__}/vendor/bin:#{ENV['PATH']}"

task default: :run

desc 'Run the bot'
task run: 'db:migrate' do
    ruby 'bin/bot'
end

desc 'Run an interactive console'
task console: 'db:migrate' do
    ruby 'bin/console'
end
