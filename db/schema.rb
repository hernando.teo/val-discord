# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161221224023) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "configurations", force: :cascade do |t|
    t.string   "key",                null: false
    t.string   "value",              null: false
    t.integer  "discord_bot_id"
    t.integer  "discord_server_id"
    t.integer  "discord_channel_id"
    t.integer  "discord_user_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["discord_bot_id"], name: "index_configurations_on_discord_bot_id", using: :btree
    t.index ["discord_channel_id"], name: "index_configurations_on_discord_channel_id", using: :btree
    t.index ["discord_server_id"], name: "index_configurations_on_discord_server_id", using: :btree
    t.index ["discord_user_id"], name: "index_configurations_on_discord_user_id", using: :btree
  end

  create_table "discord_bots", force: :cascade do |t|
    t.bigint   "bid",        null: false
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "discord_channels", force: :cascade do |t|
    t.bigint   "cid",               null: false
    t.string   "name",              null: false
    t.integer  "discord_server_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["discord_server_id"], name: "index_discord_channels_on_discord_server_id", using: :btree
  end

  create_table "discord_servers", force: :cascade do |t|
    t.bigint   "sid",            null: false
    t.string   "name",           null: false
    t.integer  "user_count"
    t.integer  "discord_bot_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["discord_bot_id"], name: "index_discord_servers_on_discord_bot_id", using: :btree
  end

  create_table "discord_servers_users", id: false, force: :cascade do |t|
    t.integer "discord_user_id",   null: false
    t.integer "discord_server_id", null: false
  end

  create_table "discord_users", force: :cascade do |t|
    t.bigint   "uid",        null: false
    t.string   "name",       null: false
    t.string   "nickname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "links", force: :cascade do |t|
    t.text     "server_id",   null: false
    t.text     "description", null: false
    t.text     "link",        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "quotes", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "quote",      null: false
    t.bigint   "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_quotes_on_user_id", using: :btree
  end

end
