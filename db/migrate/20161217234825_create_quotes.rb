# frozen_string_literal: true

class CreateQuotes < ActiveRecord::Migration[5.0]
    def change
        create_table :quotes do |t|
            t.string  :name, null: false
            t.string  :quote, null: false
            t.belongs_to :discord_user, index: true
            t.timestamps
        end
    end
end
