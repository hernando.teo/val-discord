# frozen_string_literal: true
module VAL
    class Plugin
        private

        def event_error_handler(ev)
            yield
        rescue LoadError => e
            @log.error(e.message)
        rescue Disabled
            @log.info("Event #{ev} of plugin #{@name} is disabled, skipping.")
        end

        def define_event(ev, config)
            config.types.each do |type|
                ev_type, attributes = type.is_a?(Hash) ? type.first : [type, {}]

                handler = method(ev_type).call(attributes) do |event|
                    send(config.func, event)
                end

                @events[ev] ||= {}
                @events[ev][type] = handler
            end
        end

        def load_event(ev, config)
            config = load_config(ev, config, type: :ev)
            raise Disabled unless config.enabled

            require "#{@path}/#{config.file}"

            define_event(ev, config)
        end

        def load_events
            @events_config.each do |ev, conf|
                event_error_handler(ev) { load_event(ev, conf) }
            end
        end
    end
end
