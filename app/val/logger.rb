# frozen_string_literal: true

require 'logging'

module VAL
    Logging.init Config[:logger][:levels]

    Logging.color_scheme(Config[:logger][:colorscheme], Config[:logger][:colors])

    Logging.appenders.stdout(
        'stdout',
        layout: Logging.layouts.pattern(
            pattern: Config[:logger][:format],
            color_scheme: Config[:logger][:colorscheme]
        )
    )

    rootlog = Logging.logger.root
    rootlog.level = Config[:logger][:level]
    rootlog.appenders = 'stdout'

    Config[:logger][:files].each do |name, items|
        rootlog.info("adding logfile appender: #{name}")

        filename = "#{Config[:logger][:dir]}/#{name}#{Config[:logger][:ext]}"

        Logging.appenders.rolling_file(
            name,
            filename: filename,
            roll_by: 'date',
            age: 'daily',
            layout: Logging.layouts.parseable.new(
                style: Config[:logger][:type],
                items: items
            )
        )
    end

    rootlog.appenders = %w(stdout general)
    rootlog.info('Logger initialized')
    rootlog.info("Loglevel set to '#{Config[:logger][:level]}'")
    rootlog.info("Using colorscheme '#{Config[:logger][:colorscheme]}'")
end

module VAL
    class Logger < Discordrb::Logger
        def self.[](name, **options)
            new(name, options)
        end

        def initialize(name, lvl: nil, targets: [:stdout, :general], forward: false)
            @logger           = Logging.logger[name]
            @logger.level     = lvl || Config[:logger][:level]
            @logger.appenders = targets.map(&:to_s)
            @logger.additive  = forward
        end

        Config[:logger][:levels].each do |lvl|
            define_method(lvl) { |msg| @logger.send(lvl, msg) }
        end

        def trace(e)
            exception(e.inspect)
            e.backtrace.each { |bt| exception(bt) }
        end

        alias log_exception trace
    end
end
