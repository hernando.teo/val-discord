# frozen_string_literal: true

require 'tempfile'

module VAL
    CACHE_DIR = File.absolute_path(File.join(ROOT, 'tmp/cache')).freeze

    def self.create_cache_dir(dirname)
        dir = File.join(CACHE_DIR, dirname)
        FileUtils.makedirs(dir) unless Dir.exist?(dir)
        dir
    end

    def self.tmpfile(dirname, file)
        ext  = File.extname(file)
        name = File.basename(file, ext)
        Tempfile.new([name, ext], dirname)
    end
end
