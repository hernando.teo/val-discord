# frozen_string_literal: true
module VAL
    PermissionError = Class.new(StandardError)

    def self.permitted?(event, permission: {})
        if VAL::Config[:bot][:dev_mode]
            begin
                dev?(event.author)
            rescue PermissionError => e
                raise e, 'The bot is currently in developer mode.'
            end
        end

        return true if permission.empty?

        channel = event.channel.name

        permitted = true
        permitted &&= user_permitted?(event.author, permission[:user]) if permission.key?(:user)
        permitted &&= channel_permitted?(channel, permission[:channel]) if permission.key?(:channel)
        permitted
    end

    def self.user_permitted?(author, users)
        return false unless users.is_a?(Array)

        permitted = true

        users.each do |perm|
            permitted &&=
                case perm
                when 'dev' then dev?(author)
                when 'mod' then mod?(author)
                end
        end

        permitted
    end

    def self.dev?(author)
        user_ids = VAL::Config[:bot][:permitted_ids]

        permitted = user_ids.include?(author.id)

        msg = "I'm sorry #{author.name}, but I can't let you do that."
        raise PermissionError, msg unless permitted

        permitted
    end

    def self.mod?(author)
        permitted = author.permission?(:manage_messages)

        msg = 'Insufficient permissions.'
        raise PermissionError, msg unless permitted

        permitted
    end

    def self.channel_permitted?(channel, channels)
        return false unless channels.is_a?(Array)

        permitted = channels.include?(channel)

        msg = "Only allowed in the #{channels.join(',')} channels."
        raise PermissionError, msg unless permitted

        permitted
    end
end
