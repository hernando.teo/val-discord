# frozen_string_literal: true

require 'active_record'

module VAL
    DatabaseInitError = Class.new(StandardError)

    module Database
        @log       = VAL::Logger['Database']
        @env       = ENV['KYOUKO_ENV'] || 'development'
        @db_config = nil

        def self.load_config
            @log.info("Database environment set to #{@env}")

            @db_config = VAL::Config[:database]
            raise DatabaseInitError, 'No database config!' if @db_config.nil?
        end

        def self.connect_db
            ActiveRecord::Base.establish_connection(@db_config[@env])
            @log.info('Successfully connected to database')
        end

        def self.load_models
            models_dir = File.join(VAL::ROOT, 'app/models')

            Dir["#{models_dir}/*.rb"].each do |file|
                require_relative file
                @log.info("loaded model: #{File.basename(file, '.rb')}")
            end
        end

        load_config
        connect_db
        load_models
    end
end
