# frozen_string_literal: true
module VAL
    class Plugin
        private

        def command_error_handler(cmd, event: nil)
            yield
        rescue LoadError => e
            @log.error(e.message)
        rescue Disabled
            @log.info("Command #{cmd} of plugin #{@name} is disabled, skipping.")
        rescue PermissionError => e
            @log.warn("User #{event.author.name} tried to use the #{cmd} command!")
            event << e.message
        rescue NoMethodError => e
            @log.trace(e)
            event << '**w**ell, **a**in\'t **t**hat great, an error occurred.'
        end

        def define_command(cmd, config)
            if config.options.key?(:usage)
                config.options[:usage] = format(config.options[:usage], pfx: @bot.prefix, cmd: cmd)
            end

            command cmd, config.options do |event, *args|
                command_error_handler(cmd, event: event) do
                    VAL.permitted?(event, permission: config.permission)
                    event.channel.start_typing
                    send(config.func, event, *args)
                end
            end
        end

        def load_command(cmd, config)
            config = load_config(cmd, config, type: :cmd)
            raise Disabled unless config.enabled

            require "#{@path}/#{config.file}"

            define_command(cmd, config)
        end

        def load_commands
            @commands_config.each do |cmd, config|
                command_error_handler(cmd) { load_command(cmd, config) }
            end
        end
    end
end
