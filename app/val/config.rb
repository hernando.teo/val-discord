# frozen_string_literal: true

require 'erb'
require 'yaml'

module VAL
    ROOT = File.absolute_path(File.join(__dir__, '../..'))

    module Config
        @config = {}

        def self.[](key)
            @config.fetch(key, nil)
        end

        def self.load_yaml(path)
            Dir["#{path}/*.yml"].map do |file|
                key = File.basename(file, '.yml').to_sym
                @config[key] ||= {}

                erb = ERB.new(File.read(file))
                yaml = YAML.safe_load(erb.result, [Symbol], [], true)

                @config[key].update(yaml)
            end
        end

        def self.load_defaults
            load_yaml(File.join(ROOT, 'config/default'))
        end

        def self.load_overrides
            load_yaml(File.join(ROOT, 'config'))
        end

        def self.verify_config
            raise 'Authentification token required!' if @config[:bot][:auth][:token].nil?
            raise 'Client ID required!' if @config[:bot][:auth][:token].nil?
            @config[:bot][:auth][:prefix] ||= '!'
            @config[:bot][:prefix] ||= @config[:bot][:auth][:prefix]
            @config[:bot][:game]   ||= "#{@config[:bot][:prefix]}help"
            @config[:bot][:state]  ||= :invisible
        end

        load_defaults
        load_overrides
        verify_config
    end
end
