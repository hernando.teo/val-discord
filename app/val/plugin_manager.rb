# frozen_string_literal: true
require 'yaml'
require 'erb'
require_relative 'plugin'

module VAL
    module Plugins
        Embed       = Discordrb::Webhooks::Embed
        EmbedFooter = Discordrb::Webhooks::EmbedFooter
    end

    class PluginManager
        attr_reader :plugins
        attr_reader :plugins_enabled

        def initialize(bot)
            @bot             = bot
            @plugin_config   = {}
            @plugins         = {}
            @plugins_enabled = {}

            @log = VAL::Logger['Plugin Manager']

            find_plugins
            load_plugins
        end

        def disable_plugin(name)
            return unless plugin_enabled?(name)

            @plugins_enabled[name].disable
            @plugins_enabled.delete(name)
        end

        def enable_plugin(name)
            return if plugin_enabled?(name)
            return unless plugin_loaded?(name)

            @plugins[name].enable
            @plugins_enabled[name] = @plugins[name]
        end

        def plugin_enabled?(name)
            @plugins_enabled.key?(name)
        end

        def plugin_loaded?(name)
            @plugins.key?(name)
        end

        private

        def load_plugin(path)
            plugin = Plugin.from_path(self, @bot, path)
            @plugins[plugin.name] = plugin
            @plugins_enabled[plugin.name] = plugin
        rescue Plugin::ConfigError => e
            @log.error(e.message)
        rescue Plugin::Disabled => e
            @log.info(e.message)
        end

        def load_plugins
            @log.info('Loading plugins...')

            @plugin_dirs.each do |plug_dir|
                load_plugin(plug_dir)
            end

            @log.info("Loaded #{@plugins_enabled.size} plugins.") if @plugins_enabled
        end

        def find_plugins
            plugin_dir = File.join(VAL::ROOT, 'app/plugins')

            @plugin_dirs = Dir["#{plugin_dir}/**/plugin.yml"].map do |file|
                File.dirname(file)
            end
        end
    end
end
