# frozen_string_literal: true

module Discordrb::Events
    module Respondable
        def send_temporary_embed(content = '', timeout = nil, embed = nil, &block)
            channel.send_temporary_embed(content, timeout, embed, &block)
        end
    end
end
