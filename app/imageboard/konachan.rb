# frozen_string_literal: true
module Konachan
    require 'imageboard'
    require 'open-uri'
    require 'json'

    Imageboard.board :konachan do |args|
        url = 'http://konachan.com/post.json?tags='
        url = URI.escape("#{url}#{args[:tags].join('+')}")
        tmp = []

        begin
            web = open(url)
            case web
            when Tempfile then json = JSON.parse(web.read)
            when StringIO then json = JSON.parse(web.string)
            end

            tmp = json.map { |elem| elem['file_url'] } unless json.nil?
        rescue OpenURI::HTTPError
            puts "failed to retrieve url:\n#{url}"
        end

        tmp
    end
end
