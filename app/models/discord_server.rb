# frozen_string_literal: true

class DiscordServer < ActiveRecord::Base
    validates :sid,  presence: true, uniqueness: true
    validates :name, presence: true

    has_many :discord_channels

    has_many :discord_memberships
    has_many :discord_users, through: :discord_memberships
end
