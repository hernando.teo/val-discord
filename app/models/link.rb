# frozen_string_literal: true
require 'active_record'

class Link < ActiveRecord::Base
    validates :description, presence: true
    validates :link, presence: true
end
