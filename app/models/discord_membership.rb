# frozen_string_literal: true

class DiscordMembership < ActiveRecord::Base
    belongs_to :discord_server
    belongs_to :discord_user
end
