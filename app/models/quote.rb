# frozen_string_literal: true

class Quote < ActiveRecord::Base
    validates :name, presence: true
    validates :quote, presence: true

    belongs_to :discord_user

    has_many :aliases, class_name: 'Quote'
end
