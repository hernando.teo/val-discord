# frozen_string_literal: true

class DiscordUser < ActiveRecord::Base
    validates :uid,  presence: true, uniqueness: true
    validates :name, presence: true

    has_many :discord_memberships
    has_many :discord_servers,  through: :discord_memberships

    has_many :discord_channels, through: :discord_servers
end
