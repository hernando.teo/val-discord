# frozen_string_literal: true

class VAL::Plugins::Core < VAL::Plugin
    def initialize(config)
        super(config)
        @stats = {}
    end

    private

    def show_stats(log: nil)
        load_static if @stats.empty?
        load_stats

        title_length = @stats.keys.max_by(&:size).size

        tmp = @stats.inject([]) do |acc, elem|
            acc << "#{elem.first.ljust(title_length)}  \"#{elem[1].join(' / ')}\""
        end

        tmp.each { |x| log.info(x) } unless log.nil?
        tmp.inject('') { |acc, elem| acc + "#{elem}\n" }
    end

    def load_static
        discordrb_gem = Gem::Specification.find_by_name('discordrb')

        @stats.update(
            'VAL Version/Codename:' => [VAL::Config[:bot][:version],
                                        VAL::Config[:bot][:codename]],
            'Discordrb Version:'    => [discordrb_gem.version],
            'Ruby Version:'         => ["#{RUBY_ENGINE} #{RUBY_VERSION}"],
            'Bot ID:'               => [VAL::Config[:bot][:auth][:client_id]],
            'Authors:'              => [VAL::AUTHORS.join(', ')],
            'License:'              => [VAL::Config[:bot][:license]],
            'Source:'               => [VAL::Config[:bot][:homepage]]
        )
    end

    def load_stats
        @stats.update(
            'Login Name:'             => [@bot.profile.name],
            'Server Count:'           => [@bot.servers.size],
            'User Count:'             => [@bot.users.size],
            'Plugins Enabled/Loaded:' => [@manager.plugins_enabled.size,
                                          @manager.plugins.size]
        )
    end
end
