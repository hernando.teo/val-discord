# frozen_string_literal: true

class VAL::Plugins::Core
    def cmd_invite(event)
        bot_id = VAL::Config[:bot][:auth][:client_id]
        url = 'https://discordapp.com/oauth2/authorize'
        url = "#{url}?client_id=#{bot_id}&scope=bot&permissions=0"

        event << 'Follow this link to add the bot to a server.'
        event << url
    end
end
