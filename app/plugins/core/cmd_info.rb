# frozen_string_literal: true

class VAL::Plugins::Core
    def cmd_info(event)
        event << '```haskell'
        event << show_stats
        event << '```'
    end
end
