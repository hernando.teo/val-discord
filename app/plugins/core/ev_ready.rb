# frozen_string_literal: true

class VAL::Plugins::Core
    def ev_ready(event)
        game = VAL::Config[:bot][:game]
        @log.info("setting game to #{game}")
        event.bot.game = game

        state = VAL::Config[:bot][:state]
        @log.info("setting state to #{state}")
        event.bot.send(state)

        show_stats(log: @log)
    end
end
