# frozen_string_literal: true

class VAL::Plugins::Core
    def cmd_eval(event, *args)
        event << eval(args.join(' '))
    rescue Exception => e
        event << e.message
        @log.trace(e)
        nil
    end
end
