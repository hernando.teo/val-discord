# frozen_string_literal: true
require 'rubydoc'

module VAL::Plugins
    class Ruby < VAL::Plugin
        def cmd_rubydoc(_event, *args)
            gemname = args.first
            RubyDoc.url(gemname) unless gemname.nil? || gemname.empty?
        end
    end
end
