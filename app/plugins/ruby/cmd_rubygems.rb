# frozen_string_literal: true
require 'open-uri'
require 'json'

module VAL::Plugins
    class Ruby < VAL::Plugin
        BASE_URL = 'https://rubygems.org/api/v1/search.json'
        RUBYGEMS_API_KEY = VAL::Config[:apis][:rubygems]
        RUBYGEMS_PAGE_SIZE = 5

        def cmd_rubygems(event, *args)
            results = fetch_geminfo(args.first)
            return 'Found nothing.' if results.empty?

            page_index = args[1].to_i - 1 unless args[1].nil?
            page_index ||= 0
            results_page = results[page_index]
            return if results_page.nil? || results_page.empty?

            event << list_header(results, page_index)
            event << list_gems(results_page)
        end

        private

        def fetch_geminfo(query)
            @rubygems_cache ||= {}

            return @rubygems_cache[query] unless @rubygems_cache[query].nil?

            url = "#{BASE_URL}?query=#{query}"
            web = open(url, 'Authorization' => RUBYGEMS_API_KEY)

            result =
                case web
                when StringIO then web.string
                when Tempfile then web.read
                end

            parsed = VAL::Utils.distribute(JSON.parse(result), RUBYGEMS_PAGE_SIZE)

            @rubygems_cache[query] = parsed
        end

        def list_gems(content)
            content.inject('') do |acc, result|
                acc << "```\n"
                acc << "#{result['name']} #{result['version']} by #{result['authors']}\n\n"
                acc << "#{result['info']}\n\n"
                acc << "Homepage:      <#{result['project_uri']}>\n"
                acc << "Documentation: <#{result['documentation_uri']}>\n"
                acc << '```'
            end
        end

        def list_header(results, page_index)
            results_page = results[page_index]
            results_count = results.flatten.size
            from = RUBYGEMS_PAGE_SIZE * page_index
            to = from + results_page.size
            msg = '=' * 5
            msg << " Page #{page_index + 1} of #{results.size} "
            msg << '=='
            msg << " Showing #{from + 1} to #{to} of #{results_count} "
            msg << '=' * 5
        end
    end
end
