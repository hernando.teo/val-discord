# frozen_string_literal: true
module VAL::Plugins
    class Quotes < VAL::Plugin
        def cmd_quote(_event, name, number = 1)
            quotes = Quote.where(name: name)

            if quotes.empty?
                "No quote found for #{name}"
            elsif number.to_i <= quotes.size
                quotes[number.to_i - 1].quote
            else
                quotes.last.quote
            end
        end

        def cmd_quoteadd(_event, name, *quote)
            quote = Quote.new do |q|
                q.name = name
                q.quote = quote.join(' ')
            end

            if quote.save
                'Quote added.'
            else
                "Failed to add quote for #{name}."
            end
        end

        def cmd_quotedel(_event, name, number = 1)
            quotes = Quote.where(name: name)

            return "No quotes found for #{name}" unless quotes

            quote = quotes[number.to_i - 1]

            if quote.nil?
                "No quote for #{name} at this index."
            else
                quote.destroy
                'Quote removed.'
            end
        end

        def cmd_quotes(event, name)
            quotes = Quote.where(name: name)

            return "No quotes found for #{name}." if quotes.empty?

            quotes.each.with_index(1) do |q, i|
                event << "#{i}.  #{q.quote}"
            end

            nil
        end
    end
end
