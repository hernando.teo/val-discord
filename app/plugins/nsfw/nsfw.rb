# frozen_string_literal: true
require 'imageboard'

module VAL::Plugins
    class NSFW < VAL::Plugin
        def cmd_gelbooru(event, *tags)
            tmp = Imageboard.call(:gelbooru, tags: tags)
            event << (tmp.empty? ? 'Found nothing.' : tmp.sample)
        end

        def cmd_hentaifoundry(event, *tags)
            tmp = Imageboard.call(:hentaifoundry, tags: tags)
            event << (tmp.empty? ? 'Found nothing.' : tmp.sample)
        end

        def cmd_konachan(event, *tags)
            tmp = Imageboard.call(:konachan, tags: tags)
            event << (tmp.empty? ? 'Found nothing.' : tmp.sample)
        end

        def cmd_rule34(event, *tags)
            tmp = Imageboard.call(:rule34, tags: tags)
            event << (tmp.empty? ? 'Found nothing.' : tmp.sample)
        end
    end
end
