# frozen_string_literal: true

require 'open-uri'

class VAL::Plugins::Utils
    def cmd_setavatar(event)
        attachment = event.message.attachments.first

        if attachment.nil? || !attachment.image?
            event << 'image attachment required'
        else
            @log.info(attachment.filename)
            @log.info(attachment.url)
            event.bot.profile.avatar = open(attachment.url)
            event << 'bot avatar image updated'
        end
    end
end
