# frozen_string_literal: true

class VAL::Plugins::Utils
    EmbedImage = Discordrb::Webhooks::EmbedImage

    def cmd_avatar(event, _args = nil)
        user =
            if event.message.mentions.empty?
                event.author
            else
                event.message.mentions.first
            end

        event.channel.send_embed do |embed|
            embed.title = user.name
            embed.image = EmbedImage.new(url: user.avatar_url)
        end
    end
end
