# frozen_string_literal: true

class VAL::Plugins::Utils
    def cmd_setnick(event, *new_name)
        bot_profile = event.bot.profile
        bot_member = event.server.member(bot_profile.id)

        new_name = new_name.join(' ')

        bot_member.nickname =
            if new_name.empty?
                nil
            else
                new_name
            end

        event << 'bot nickname updated'
    end
end
