# frozen_string_literal: true

require_relative 'music_player'
require_relative 'youtube'

class VAL::Plugins::Music
    Embed = Discordrb::Webhooks::Embed
    DEFAULT_MESSAGE_TIMEOUT = Discordrb::Channel::DEFAULT_MESSAGE_TIMEOUT

    def cmd_connect(event)
        msg =
            if event.author.voice_channel.nil?
                "User #{event.author.name} is not in a voice channel."
            else
                "Connected to voice channel #{player(event).channel.name}."
            end

        delete_origin_message(event) do |embed|
            embed.title       = @name
            embed.description = msg
        end
    end

    def cmd_reconnect(event)
        msg =
            if event.author.voice_channel.nil?
                "User #{event.author.name} is not in a voice channel."
            else
                player(event).reconnect
                "Reconnected to voice channel #{player(event).channel.name}."
            end

        delete_origin_message(event) do |embed|
            embed.title       = @name
            embed.description = msg
        end
    end

    def cmd_disconnect(event)
        with_connection(event) do
            server = event.server
            @music_players[server.id].disconnect
            @music_players.delete(server.id)
            "Disconnected voice from server #{server.name}"
        end
    end

    def cmd_play(event, *args)
        with_connection(event) do
            begin
                song = @youtube.download(args.join(' '))
            rescue YoutubeDownloader::DownloadError => e
                e.message
            else
                player(event).play(song)
                "Added <#{song}> to the playlist"
            end
        end
    end

    def cmd_pause(event)
        with_connection(event) do
            player(event).pause
            'Playback paused.'
        end
    end

    def cmd_resume(event)
        with_connection(event) do
            player(event).resume
            'Playback resumed.'
        end
    end

    def cmd_stop(event)
        with_connection(event) do
            player(event).stop
            'Playback stopped and queue cleared.'
        end
    end

    def cmd_skip(event)
        with_connection(event) do
            player(event).skip
            'Current song skipped.'
        end
    end

    def cmd_playlist(event)
        with_connection(event) do
            playlist    = player(event).playlist
            max_display = 10

            delete_origin_message(event) do |embed|
                embed.title = 'Playlist'
                embed.description = "```markdown\n"

                playlist.take(max_display).each.with_index(1) do |x, i|
                    embed.description += "#{i}.\t#{x.to_s.truncate(50)}\n"
                end

                rest = playlist.size - max_display
                embed.description += "\nand #{rest} more.\n" if rest.positive?

                embed.description += "```\n"
            end
        end
    end

    def cmd_musicinfo(event)
        event.send_temporary_embed do |embed|
            embed.title       = @name
            embed.description = player(event).inspect
        end
    end

    private

    def delete_origin_message(event, &block)
        event.send_temporary_embed(&block)

        Thread.new do
            sleep(DEFAULT_MESSAGE_TIMEOUT)
            event.message.delete
        end

        nil
    end

    def with_connection(event)
        msg =
            if event.voice.nil?
                'Not connected to any voice channels on this server.'
            else
                yield
            end

        return if msg.nil?

        delete_origin_message(event) do |e|
            e.title       = @name
            e.description = msg
        end
    end

    def player(event)
        channel = event.author.voice_channel
        @music_players[event.server.id] ||= Player.new(@bot, channel)
    end
end
