# frozen_string_literal: true

require 'youtube-dl.rb'

class VAL::Plugins::Music
    class YoutubeDownloader
        DownloadError = Class.new(StandardError)

        CACHE_DIR = VAL.create_cache_dir('music/ytdl')

        OUTFILE = File.join(CACHE_DIR, '%(id)s.%(ext)s')

        MAX_RETRIES = 3

        OPTIONS = {
            extract_audio:      true,
            audio_format:       'mp3',
            format:             :bestaudio,
            output:             OUTFILE,
            restrict_filenames: true,
            playlist:           false,
            write_info_json:    true,
            default_search:     'auto'
        }.freeze

        def initialize
            @log = VAL::Logger['YTDL']
        end

        def download(search)
            with_retries do |tries|
                @log.info("Downloading (try #{tries}/#{MAX_RETRIES}): #{search}")

                fetch_video(search)
            end
        rescue DownloadError
            raise DownloadError, "Failed to download #{search}."
        end

        private

        def fetch_video(search)
            video    = YoutubeDL::Video.new(search, OPTIONS)
            ext      = File.extname(video.filename)
            basename = File.basename(video.filename, ext)
            dlfile   = File.join(CACHE_DIR, "#{basename}.mp3")

            video.download unless File.exist?(dlfile)

            song = Song.new(
                file: dlfile,
                title: video.information[:fulltitle],
                duration: video.information[:duration]
            )

            @log.info("Downloaded: #{song.file}")

            song
        end

        def with_retries
            tries = 1

            begin
                yield(tries)
            rescue Cocaine::ExitStatusError
                raise DownloadError if tries == MAX_RETRIES
                tries += 1
                retry
            end
        end
    end
end
