# frozen_string_literal: true

class VAL::Plugins::Music
    class Song
        attr_reader :title
        attr_reader :file
        attr_reader :duration

        def initialize(file: nil, title: nil, duration: 0)
            @file     = file
            @title    = title || 'Unknown title'
            @duration = duration
        end

        def to_s
            @title
        end
    end
end
