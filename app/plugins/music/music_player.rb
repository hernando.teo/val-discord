# frozen_string_literal: true

require 'thread'
require_relative 'song'
require_relative 'playlist'

class VAL::Plugins::Music
    class Player
        attr_reader   :channel
        attr_accessor :error

        def initialize(bot, channel)
            @bot          = bot
            @channel      = channel
            @mutex        = Mutex.new
            @reconnecting = ConditionVariable.new
            @queue        = Queue.new
            @playlist     = Playlist.new
            @current_file = nil
            @error        = nil
            @replay       = false
            @skip_time    = 0

            connect_internal
            start_playloop
        end

        def play(item)
            @playlist << item
            @queue << item.file
        end

        def stop
            @vbot.stop_playing
            @current_file = nil
        end

        def pause
            @vbot.pause
        end

        def resume
            @vbot.continue
        end

        def destroy
            stop
            @vbot.destroy
        end

        def skip
            with_lock do
                @vbot.stop_playing
                @current_file = nil
            end
        end

        def play_time
            @vbot&.stream_time || 0
        end

        def reconnect
            reconnect_internal
        end

        def disconnect
            disconnect_internal
            @queue.clear
            @playlist.clear
        end

        def playing?
            @vbot&.playing? || false
        end

        def connected?
            return false if error == :disconnect
            !@vbot.nil?
        end

        def playlist
            @playlist.to_a
        end

        def inspect
            "<ch: #{@channel.name}, replay: #{@replay}, skiptime: #{@skip_time}>"
        end

        private

        def connect_internal
            @vbot = @bot.voice_connect(@channel)
        end

        def disconnect_internal
            @vbot.destroy
            @vbot = nil
        end

        def reconnect_internal
            @skip_time = @vbot.stream_time
            @replay = true

            disconnect_internal
            connect_internal
        end

        def start_playloop
            @playloop = Thread.new do
                loop do
                    next if playing?
                    next unless connected?

                    @current_file ||= @queue.deq
                    @replay = false

                    @vbot&.skip(@skip_time)
                    @vbot&.play_file(@current_file)

                    next if @replay
                    next unless @error.nil?

                    @skip_time = 0
                    @playlist.shift
                    @current_file = nil
                end
            end
        end

        def with_lock
            @mutex.synchronize do
                yield
            end
        end

        def wait_for(cond)
            sleep(0.5) until cond
            yield
        end
    end
end
