# frozen_string_literal: true

class VAL::Plugins::Music < VAL::Plugin; end

require_relative 'youtube'

class VAL::Plugins::Music
    def initialize(config)
        super(config)

        @music_players = {}
        @youtube = YoutubeDownloader.new
    end
end
