# frozen_string_literal: true

class VAL::Plugins::Music
    def ev_disconnected(_event)
        @music_players.values.each do |music_player|
            music_player.error = :disconnect
        end
    end
end
