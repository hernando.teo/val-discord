# frozen_string_literal: true

require 'thread'

class VAL::Plugins::Music
    class Playlist
        def initialize(list = [])
            @list    = list
            @mutex   = Mutex.new
        end

        def push(item)
            with_lock { @list.push(item) }
        end

        alias enq push
        alias << push

        def shift
            with_lock { @list.shift }
        end

        alias deq shift

        def clear
            with_lock { @list.clear }
        end

        def first
            with_lock { @list.first }
        end

        def to_a
            with_lock { @list }
        end

        def to_s
            to_a.to_s
        end

        private

        def with_lock
            @mutex.synchronize do
                yield
            end
        end
    end
end
