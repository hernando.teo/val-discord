# frozen_string_literal: true
module VAL::Plugins
    class Misc
        def cmd_bmi(event, weight, height)
            w = weight.to_f
            h = height.to_f
            bmi = (w / (h * h))

            event << format('Your BMI is %.2f', bmi)
            event << "BMI Classification: #{classification(bmi)}"
        end

        private

        def classification(bmi)
            if bmi >= 40.00
                'Morbid Obesity'
            elsif bmi.between?(35.00, 40.00)
                'Obesity (Class 2)'
            elsif bmi.between?(30.00, 35.00)
                'Obesity (Class 1)'
            elsif bmi.between?(25.00, 30.00)
                'Overweight'
            elsif bmi.between?(18.50, 25.00)
                'Normal Weight'
            elsif bmi <= 18.50
                'Underweight'
            end
        end
    end
end
