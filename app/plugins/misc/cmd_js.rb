# frozen_string_literal: true
module VAL::Plugins
    class Misc
        def cmd_js(_event, *args)
            to_eval = args.join(' ')
            IO.popen("node --print '#{to_eval}'").readlines.join.chomp
        end
    end
end
