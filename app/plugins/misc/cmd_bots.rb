# frozen_string_literal: true
module VAL::Plugins
    class Misc
        def cmd_bots(event)
            bots = event.server.users.select(&:bot_account?)

            event.channel.send_embed do |embed|
                embed.title = "Bots\n"
                bots.each do |b|
                    embed.add_field name: b.name, value: b.status, inline: false
                end
            end
        end
    end
end
