# frozen_string_literal: true
require 'tzinfo'

module VAL::Plugins
    class Misc
        def cmd_tzinfo(_event, country)
            zones = TZInfo::Country.get(country).zone_identifiers
            TZInfo::Timezone.get(zones.first).now
        rescue TZInfo::InVALidCountryCode
            "Can't find any zones with the country code `#{country}`"
        end
    end
end
