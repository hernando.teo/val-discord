# frozen_string_literal: true

module VAL::Plugins
    class Misc
        def ev_server_created(event)
            server = event.server.name
            @log.info("Joined #{server}")
        end
    end
end
