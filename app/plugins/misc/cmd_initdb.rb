# frozen_string_literal: true

module VAL::Plugins
    class Misc
        def cmd_initdb(_event)
            init_servers(init_bot)
            'Done.'
        rescue => e
            @log.trace(e)
        end

        private

        def init_bot
            DiscordBot.where(bid: @bot.profile.id).first || create_bot
        end

        def create_bot
            DiscordBot.create do |b|
                b.bid  = @bot.profile.id
                b.name = @bot.profile.name
            end
        end

        def init_servers(ar_bot)
            @bot.servers.each do |server_id, server|
                ar_bot.discord_servers << init_server(server_id, server)
            end
        end

        def init_server(server_id, server)
            ar_server = DiscordServer.create do |s|
                s.sid         = server_id
                s.name        = server.name
                s.user_count  = server.member_count
            end

            server.members.each do |member|
                ar_server.discord_users << init_member(member)
            end

            server.channels.each do |channel|
                ar_server.discord_channels << init_channel(channel)
            end
        rescue ActiveRecord::RecordInvalid
            @log.info("Server #{server.name} is already in database.")
        end

        def init_member(member)
            DiscordUser.where(uid: member.id).first || create_member(member)
        end

        def init_channel(channel)
            DiscordChannel.where(cid: channel.id).first || create_channel(channel)
        end

        def create_member(member)
            DiscordUser.create do |u|
                u.uid  = member.id
                u.name = member.name
            end
        end

        def create_channel(channel)
            DiscordChannel.create do |c|
                c.cid  = channel.id
                c.name = channel.name
            end
        end
    end
end
