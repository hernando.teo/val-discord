# frozen_string_literal: true
module Imageboard
    class Board
        def initialize(&block)
            @block = block
        end

        def call(arguments = {})
            @arguments = {
                tags: [arguments[:tags]].flatten.map(&:to_s) || [],
                page: arguments[:page] || 1
            }

            @block.call(@arguments)
        end
    end

    def self.board(name, &block)
        @boards ||= {}
        @boards[name] = Board.new(&block)
    end

    def self.call(board_name, arguments = {})
        if @boards[board_name].nil?
            []
        else
            @boards[board_name].call(arguments)
        end
    end

    files = Dir["#{File.dirname(__FILE__)}/imageboard/*.rb"]
    files.each { |file| require file }
    BOARDS = files.map { |file| File.basename(file, '.rb').to_sym }
end
